# 基于springboot+electron+vue的轻量级桌面应用后台管理框架（ht_electron）

#### Description
基于springboot+electron+vue的轻量级桌面应用后台管理框架（ht_electron）,框架内容比较基础，没有过多的冗余内容,
实现了前后端的用户和权限模块的相关内容，框架还不够完善用于学习使用，有问题欢迎大家指出，相互学习

### 内置基础功能
1. 登录
2. 菜单管理：配置系统菜单，操作权限，按钮权限标识等。
3. 角色管理：用户角色管理，为每一个角色设置不同的菜单权限已经不同的数据权限规则
4. 用户管理: 用户实现，多角色实现。
5. 字典管理：字典表维护。
6. 部门管理：管理用户所在部门，并实现数据权限，通过mapper接口配置DataScope参数实现自动数据权限过滤

#### 技术说明
1. Sping-boot+Spring+Sprng-mvc+MyBatis-Plus做的基础功能
2. 使用jwt生成token实现前后端分离的权限校验
3. 引入druid连接池，swagger
4. spring security作为权限控制，实现多角色权限管理
5. 前端框架Vue+Element-ui+Avue+electron
6. 实现动态路由来生成用户菜单，结合security实现按钮权限以及按钮的显示隐藏
7. 前端表格框架使用auve简化对表格的开发,
8. 对axios进行二次封装，统一管理接口请求，统一错误处理
9. 统一实现定时任务管理-通过继承ScheduledOfTask来实现定时任务


#### 环境准备
1. npm config edit
2. 该命令会打开npm的配置文件，请在空白处添加，此操作是配置淘宝镜像。
3. registry=https://registry.npmmirror.com
4. electron_mirror=https://cdn.npmmirror.com/binaries/electron/
5. electron_builder_binaries_mirror=https://npmmirror.com/mirrors/electron-builder-binaries/
6. 然后关闭该窗口，重启命令行.

##运行准备
1. mysql5.7
2. java8
3. maven
4. redis
5. node版本16以上，我的版本是16.14.1
6. 依赖安装  yarn install
7. 前端项目运行 yarn dev
8. 前端打包，yarn build
9. 不会后端的朋友可以单独运行前端，放开前端登录的路由限制就可以了，具体在router/permisstion.js
   文件中做的token校验。
10. 管理员帐号 admin/123456


#### 演示图

首页
![info](/img/sy.png)

登录
![info](/img/login.png)

用户管理
![info](/img/user.png)

角色管理
![info](/img/role.png)

菜单管理
![info](/img/menu.png)
 
字典管理
![info](/img/dict.png)

部门管理
![info](/img/dept.png)

操作日志管理
![info](/img/log.png)

定时任务管理
![info](/img/task.png)


