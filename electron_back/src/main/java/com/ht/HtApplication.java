package com.ht;

import com.ht.util.JsonResult;
import io.swagger.annotations.ApiOperation;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@SpringBootApplication
@EnableSwagger2
@MapperScan(value = "com.ht.module.*.mapper")
@EnableGlobalMethodSecurity(securedEnabled = true,prePostEnabled = true) // 控制权限注解
@Import(cn.hutool.extra.spring.SpringUtil.class)
public class HtApplication {
    public static void main(String[] args) {SpringApplication.run(HtApplication.class, args); }


}
