package com.ht.config.security.handler;

import com.alibaba.fastjson.JSON;
import com.ht.constant.BusConstant;
import com.ht.constant.RedisConstants;
import com.ht.module.sys.entity.SysMenu;
import com.ht.module.sys.entity.SysRole;
import com.ht.module.sys.entity.SysUser;
import com.ht.module.sys.service.ISysUserService;
import com.ht.module.sys.vo.SysUser.UserLoginVo;
import com.ht.util.JsonResult;
import com.ht.util.JwtUtils;
import com.ht.util.RedisUtil;
import io.lettuce.core.output.MapOutput;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @ProjectName: ht
 * @ClassName: LocalAuthenticationSuccessHandler
 * @Author: hejialun
 * @Description: 登录成功处理
 * @Date: 2021/6/21 15:35
 */
@Component
public class LocalAuthenticationSuccessHandler implements AuthenticationSuccessHandler {
    @Autowired
    private RedisUtil redisUtil;
    @Autowired
    private ISysUserService sysUserService;

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication)
            throws IOException, ServletException {
        response.setCharacterEncoding("utf-8");
        response.setContentType("application/json;charset=utf-8");
        //登录成功
        User user = (User) authentication.getPrincipal();
        UserLoginVo userLoginVo = sysUserService.loginUser(user.getUsername());
        response.getWriter().print(JSON.toJSONString(JsonResult.success(userLoginVo)));
    }

}