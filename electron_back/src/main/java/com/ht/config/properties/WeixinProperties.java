package com.ht.config.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @ClassName WeixingProperties
 * @Description TODO（微信配置信息）
 * @Author hejialun
 * @Date 2022/12/8 12:02
 * @Version 1.0
 */
@Data
@Configuration
@ConfigurationProperties(prefix = "weixin")
public class WeixinProperties {

    private String token;
}
