package com.ht.config.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @ClassName DingDingConfig
 * @Description TODO（钉钉配置信息）
 * @Author hejialun
 * @Date 2022/12/6 17:46
 * @Version 1.0
 */
@Data
@Configuration
@ConfigurationProperties(prefix = "dingding")
public class DingDingProperties {

    private String appKey;

    private String agentId;

    private String appSecret;
}
