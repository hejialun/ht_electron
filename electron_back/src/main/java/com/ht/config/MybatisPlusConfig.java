package com.ht.config;
import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import com.ht.config.dataAuth.DataScopeInterceptor;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;


/**
 * @ProjectName: ht
 * @ClassName: MybatisPlusConfig
 * @Description: 分页插件配置
 */
@Configuration
@EnableTransactionManagement
@MapperScan("com.ht.module.*.mapper")
public class MybatisPlusConfig {

    /**
     * 分页插件
     */
    @Bean
    public PaginationInterceptor paginationInterceptor() {
        return new PaginationInterceptor();
    }


    /**
     * @Author hejialun
     * @Date 15:26 2022/4/22
     * @Description TODO(数据权限插件)
     * @param
     * @return com.ht.config.dataAuth.DataScopeInterceptor
     */
    @Bean
    public DataScopeInterceptor dataScopeInterceptor() {
        return new DataScopeInterceptor();
    }

}