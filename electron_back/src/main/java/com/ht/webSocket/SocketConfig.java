package com.ht.webSocket;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.server.standard.ServerEndpointExporter;

/**
 * @ProjectName: ht
 * @ClassName: SocketConfig
 * @Author: hejialun
 * @Description: socket配置
 * @Date: 2021/9/16 11:17
 */
@Configuration
public class SocketConfig {
    @Bean
    public ServerEndpointExporter serverEndpointExporter() {
        return new ServerEndpointExporter();
    }
}