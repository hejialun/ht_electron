package com.ht.module.base.mapper;

import com.ht.module.base.entity.BaseScheduledTask;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 定时任务表 Mapper 接口
 * </p>
 *
 * @author hejialun
 * @since 2022-04-24
 */
public interface BaseScheduledTaskMapper extends BaseMapper<BaseScheduledTask> {

}
