package com.ht.module.common.service.impl;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.ht.abnormal.HtException;
import com.ht.module.common.service.CommonService;

import com.ht.util.CommMethod;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.apache.poi.hwpf.extractor.WordExtractor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.*;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @ProjectName: ht
 * @ClassName: CommonServiceImpl
 * @Author: hejialun
 * @Description: 公共类实现类
 * @Date: 2021/9/14 16:14
 */
@Service
public class CommonServiceImpl implements CommonService {

}