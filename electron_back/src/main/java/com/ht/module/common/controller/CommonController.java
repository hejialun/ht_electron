package com.ht.module.common.controller;

import com.ht.module.common.service.CommonService;
import com.ht.module.sys.service.ISysDictService;
import com.ht.util.JsonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


/**
 * @ProjectName: ht
 * @ClassName: commonController
 * @Author: hejialun
 * @Description: 公共类的控制器
 * @Date: 2021/9/14 15:50
 */
@RestController
@RequestMapping("/common")
@Api(tags = "公共类控制器 ")
public class CommonController {
    @Autowired
    private CommonService commonService;
    @Autowired
    private ISysDictService iSysDictService;

    @ApiOperation(value = "查询码表")
    @GetMapping("/getItem/{code}")
    public JsonResult getItem(@PathVariable String code){
        return JsonResult.success(iSysDictService.getItem(code));
    }


}