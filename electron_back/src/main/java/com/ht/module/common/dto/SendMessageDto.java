package com.ht.module.common.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @ProjectName: ht
 * @ClassName: SendMessageDto
 * @Author: hejialun
 * @Description: 消息发送模板
 * @Date: 2021/9/16 11:32
 */
@Data
public class SendMessageDto {
    @ApiModelProperty("消息类型:USER:用户消息，")
    @NotBlank
    private String type;

    @ApiModelProperty("消息id")
    @NotBlank
    private String id;

    @ApiModelProperty("消息内容")
    @NotBlank
    private String msg;

}