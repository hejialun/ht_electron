package com.ht.module.common.dto;

import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @ProjectName: ht
 * @ClassName: UpdateStateDto
 * @Author: hejialun
 * @Description: 修改state字段
 * @Date: 2021/9/14 16:06
 */
@Data
public class UpdateStateDto {

    @ApiModelProperty("数据库表对应编码：关联码表 tableCode")
    @NotBlank
    private String tableCode;

    @ApiModelProperty("主键id")
    @NotBlank
    private String key;

    @ApiModelProperty("修改后的值")
    @NotBlank
    private String state;
}