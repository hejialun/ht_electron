package com.ht.module.sys.controller;

import com.ht.config.log.annotation.SysLog;
import com.ht.constant.DicConstants;
import com.ht.module.sys.entity.SysRole;
import com.ht.module.sys.service.ISysRoleAuthService;
import com.ht.util.JsonResult;
import com.ht.util.Pager;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.ht.module.sys.entity.SysRoleAuth;
import com.ht.util.*;

import javax.validation.Valid;


/**
 * <p>
 * 角色自定义权限表 前端控制器
 * </p>
 *
 * @author hejialun
 * @since 2022-04-20
 */
@RestController
@RequestMapping("/sys-role-auth")
public class SysRoleAuthController {

    
    @Autowired
    private ISysRoleAuthService iSysRoleAuthService;




    @GetMapping("/findCustomByRoleId/{roleId}")
    @ApiOperation(value = "通过角色ID获取角色自定义权限")
    @SysLog(msg = "通过角色ID获取角色自定义权限")
    public JsonResult findCustomByRoleId(@PathVariable String roleId){
        return JsonResult.success(iSysRoleAuthService.findCustomByRoleId(roleId));
    }


}
