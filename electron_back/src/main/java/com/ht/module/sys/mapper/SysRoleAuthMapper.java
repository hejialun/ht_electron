package com.ht.module.sys.mapper;

import com.ht.module.sys.entity.SysRoleAuth;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 角色自定义权限表 Mapper 接口
 * </p>
 *
 * @author hejialun
 * @since 2022-04-20
 */
public interface SysRoleAuthMapper extends BaseMapper<SysRoleAuth> {

}
