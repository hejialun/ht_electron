package com.ht.module.sys.vo.SysUser;

import com.ht.module.sys.entity.SysUser;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @ClassName LoginDto
 * @Description TODO（用户登录的vo）
 * @Author hejialun
 * @Date 2022/12/7 17:22
 * @Version 1.0
 */
@Data
@Accessors(chain = true)
public class UserLoginVo {
    @ApiModelProperty("用户信息")
    private SysUser userInfo;

    @ApiModelProperty("jwtToken")
    private String token;

}
