package com.ht.module.sys.controller;

import com.ht.config.log.annotation.SysLog;
import com.ht.constant.DicConstants;
import com.ht.util.JsonResult;
import com.ht.util.Pager;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import com.ht.module.sys.entity.SysOperLog;
import com.ht.module.sys.service.ISysOperLogService;
import com.ht.util.*;

import javax.validation.Valid;


/**
 * <p>
 * 系统操作日志 前端控制器
 * </p>
 *
 * @author hejialun
 * @since 2022-04-26
 */
@RestController
@RequestMapping("/sys-oper-log")
public class SysOperLogController {

    
    @Autowired
    private ISysOperLogService iSysOperLogService;

    



	@ApiOperation("分页查询")
    @GetMapping("/findPage")
    @PreAuthorize("hasAnyAuthority('SYS_OPER_LOG')")
    public JsonResult findPage(Pager<SysOperLog> pager, SysOperLog en){
        return JsonResult.success(iSysOperLogService.findPage(pager,en));
    }


    @ApiOperation("通过id删除")
    @DeleteMapping("/delete-by-id/{id}")
    @PreAuthorize("hasAnyAuthority('SYS_OPER_LOG_DEL')")
    @SysLog(msg="删除日志",type = DicConstants.OperLogType.DEL)
    public JsonResult delete(@PathVariable(value = "id") String id){
        iSysOperLogService.removeById(id);
        return JsonResult.success();
    }

}
