package com.ht.module.sys.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ht.module.sys.entity.SysRole;
import com.ht.module.sys.entity.SysUserRole;
import com.ht.module.sys.mapper.SysRoleMapper;
import com.ht.module.sys.mapper.SysUserRoleMapper;
import com.ht.module.sys.service.ISysUserRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Auto-generator
 * @since 2021-04-27
 */
@Service
public class SysUserRoleServiceImpl extends ServiceImpl<SysUserRoleMapper, SysUserRole> implements ISysUserRoleService {
    @Resource
    private SysRoleMapper sysRoleMapper;

    @Override
    public List<SysRole> findRoleByUserId(String id) {
        return baseMapper.findRoleByUserId(id);
    }

    @Override
    public void saveSpecifiedRole(String id, String code) {
        //查询角色
        SysRole sysRole=sysRoleMapper.selectOne(
                new QueryWrapper<SysRole>()
                        .eq("code",code)
        );
        SysUserRole sysUserRole = new SysUserRole();
        sysUserRole.setRoleId(sysRole.getId());
        sysUserRole.setUserId(id);
        //保存角色
        this.save(sysUserRole);
    }
}
