package com.ht.module.sys.mapper;

import com.ht.module.sys.entity.SysRole;
import com.ht.module.sys.entity.SysUserRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Auto-generator
 * @since 2021-04-27
 */
@Mapper
public interface SysUserRoleMapper extends BaseMapper<SysUserRole> {
    /*
     * @param id:用户id
     * @Author hejialun
     * @Description: TODO(通过用户id查询用户角色)
     * @date 2021/5/12 22:00
     * @return
     */
    List<SysRole> findRoleByUserId(String id);
}
