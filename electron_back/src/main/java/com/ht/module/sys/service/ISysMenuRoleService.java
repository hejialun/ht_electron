package com.ht.module.sys.service;

import com.ht.module.sys.entity.SysMenuRole;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 权限菜单表 服务类
 * </p>
 *
 * @author Auto-generator
 * @since 2021-05-13
 */
public interface ISysMenuRoleService extends IService<SysMenuRole> {

}
