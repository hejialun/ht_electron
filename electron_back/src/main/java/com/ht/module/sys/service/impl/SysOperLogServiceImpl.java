package com.ht.module.sys.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ht.module.sys.entity.SysOperLog;
import com.ht.module.sys.mapper.SysOperLogMapper;
import com.ht.module.sys.service.ISysOperLogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ht.util.Pager;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 系统操作日志 服务实现类
 * </p>
 *
 * @author hejialun
 * @since 2022-04-26
 */
@Service
public class SysOperLogServiceImpl extends ServiceImpl<SysOperLogMapper, SysOperLog> implements ISysOperLogService {

    @Override
    public Pager<SysOperLog> findPage(Pager<SysOperLog> pager, SysOperLog en) {
        QueryWrapper<SysOperLog> qw=new QueryWrapper<>();
        if(StrUtil.isNotEmpty(en.getTitle())){
            qw.like("tab.title",en.getTitle());
        }
        if(StrUtil.isNotEmpty(en.getOperType())){
            qw.eq("tab.oper_type",en.getOperType());
        }
        return baseMapper.findPage(pager,qw);
    }
}
