package com.ht.util;


import com.ht.constant.BusConstant;
import com.ht.module.sys.entity.SysUser;
import com.ht.module.sys.service.ISysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * @ClassName: UserUtil
 * @Description: 用户工具类
 */
@Component
public class UserUtil {
    @Autowired
    private ISysUserService userService;
    @Autowired
    private static  ISysUserService iSysUserService;
    @PostConstruct
    public void init(){
        UserUtil.iSysUserService = userService;
    }



    /**
     * 获取 request 里传递的 token
     *
     * @param request
     * @return
     */
    public static String getTokenByRequest(HttpServletRequest request) {
        String token = request.getParameter(BusConstant.TOKEN);
        if (token == null) {
            token = request.getHeader(BusConstant.X_ACCESS_TOKEN);
        }
        return token;
    }

    /*
     * @Author hejialun
     * @Description: TODO(获取用户名)
     * @date 2021/5/14 20:30
     * @return
     */
    public static String getUserName() {
        HttpServletRequest request =((ServletRequestAttributes)RequestContextHolder.getRequestAttributes()).getRequest();
        String username = JwtUtils.getKey(getTokenByRequest(request),"username");
        return username;
    }

    /*
     * @Author hejialun
     * @Description: TODO(获取用户信息)
     * @date 2021/5/14 20:30
     * @return
     */
    public static SysUser getUser() {
        HttpServletRequest request =((ServletRequestAttributes)RequestContextHolder.getRequestAttributes()).getRequest();
        String username = JwtUtils.getKey(getTokenByRequest(request),"username");
        SysUser user = iSysUserService.getUserInfoByUsername(username);
        return user;
    }


    /*
     * @Author hejialun
     * @Description: TODO(获取用户id)
     * @date 2021/5/14 20:30
     * @return
     */
    public static String getUserId() {
        HttpServletRequest request =((ServletRequestAttributes)RequestContextHolder.getRequestAttributes()).getRequest();
        return JwtUtils.getKey(getTokenByRequest(request),"id");
    }




}