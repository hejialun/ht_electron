package com.ht.util.ssh;

import lombok.Data;

@Data
public class SshConnection {
    private String ip;
    private Integer port;
    private String username;
    private String password;

    public SshConnection(String ip, Integer port, String username, String password) {
        this.ip = ip;
        this.port = port;
        this.username = username;
        this.password = password;

    }


}

