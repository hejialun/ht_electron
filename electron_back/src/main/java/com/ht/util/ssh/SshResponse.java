package com.ht.util.ssh;

import lombok.Data;
import java.util.Arrays;
import java.util.List;

@Data
public class SshResponse {

    //成功返回字符串
    private String stdOutput;
    //成功返回集合
    private List<String> stdOutputArray;
    //异常返回
    private String errOutput;
    //异常返回集合
    private List<String> errOutputArray;
    //返回编码：0成功；1失败
    private int returnCode;
    //成功信息长度
    private int successLength;
    //失败信息长度
    private int errorLength;


    SshResponse(String stdOutput, String errOutput, int returnCode) {
        this.stdOutput = stdOutput;
        this.errOutput = errOutput;
        this.returnCode = returnCode;
        this.stdOutputArray = Arrays.asList(stdOutput.toString().split("\\n"));
        this.errOutputArray = Arrays.asList(errOutput.toString().split("\\n"));
        this.successLength = stdOutputArray.size();
        this.errorLength = errOutputArray.size();
    }

    /**
     * @Author hejialun
     * @Date 18:43 2022/11/21
     * @Description TODO(获取成功最后一个打印信息)
     * @param
     * @return java.lang.String
     */
    public String getSuccessEndInfo(){
        return this.stdOutputArray.get(this.successLength-1);
    }

}
