package com.ht.task;

import com.ht.config.task.ScheduledOfTask;
import com.ht.module.base.entity.BaseScheduledTask;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @ClassName HelloTask
 * @Description TODO（问好定时任务-用于测试）
 * @Author hejialun
 * @Date 2022/4/24 15:03
 * @Version 1.0
 */
@Component
@Slf4j
public class HelloTask implements ScheduledOfTask {
    @Override
    public void execute(BaseScheduledTask  st) {
        log.info("----{}----",st.getTaskName());
    }
}
