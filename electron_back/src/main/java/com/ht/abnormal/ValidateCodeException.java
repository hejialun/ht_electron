package com.ht.abnormal;

import org.springframework.security.core.AuthenticationException;

/**
 * @ProjectName: ht
 * @ClassName: ValidateCodeException
 * @Author: hejialun
 * @Description: 验证码异常
 * @Date: 2021/5/18 23:42
 */
public class ValidateCodeException extends AuthenticationException {
    public ValidateCodeException(String msg) {
        super(msg);
    }

}