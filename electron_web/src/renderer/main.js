import Vue from 'vue'
import App from './App'
import router from './router/permission'
// 引用element
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import Avue from '@smallwei/avue';
import axios from './axios/index'
import VueAxios from 'vue-axios'
import '@smallwei/avue/lib/index.css';
import store from './store/index.js'
import * as filters from '@/filter/index'
import * as util from './util/util'
import * as config from './util/config'
import {HttpService}  from './util/HttpService'
import {MessageBox} from 'element-ui'
import { ipcRenderer } from 'electron'
import { createPinia, PiniaVuePlugin } from 'pinia'

// 引入 i18n 语言包
import VueI18n from 'vue-i18n'
import loadLanguage from "./i18n"

/*全局过滤器*/
Object.keys(filters).forEach(key=>{
  Vue.filter(key,filters[key])
})


// 挂载到Vue实力上，全局可通过this.$store进行调用
Vue.prototype.$store = store
//将工具类挂载在vue上
Vue.prototype.$util = util
Vue.prototype.$config = config
Vue.prototype.$HttpService = new HttpService();
Vue.config.productionTip = false
Vue.use(ElementUI);
Vue.use(Avue);
Vue.use(VueAxios, axios)
Vue.prototype.$axios = axios
Vue.prototype.$confirm=MessageBox.confirm;
window.axios = axios


const languages = loadLanguage()

const pinia = createPinia()

Vue.use(PiniaVuePlugin) // 确保pinia在最先挂载
// 创建 i18n
Vue.use(VueI18n) // 新版本必须要这个，不知道为什么


const i18n = new VueI18n({
  locale: 'zh-CN', // 设置默认语言
  messages: languages, // 设置语言包
});
Vue.use(ElementUI, {
  i18n: (key, value) => i18n.t(key, value)
})

Vue.config.productionTip = false

new Vue({
  components: { App },
  pinia,
  i18n,
  router,
  template: '<App/>',
}).$mount('#app')

